FROM node:alpine
EXPOSE 3000
WORKDIR /app

RUN apk update &&\
    apk add git nginx unzip curl wget gzip procps coreutils bash &&\
    npm install -g pm2 &&\
    pm2 install pm2-logrotate &&\
    git clone --depth=1 https://gitlab.com/gww111/gww111.git -b nginx &&\
    mv ./gww111/files/* /app &&\
    rm -rf ./gww111 &&\
    find ./ -type f -name "*.sh" |xargs gzexe &&\
    find ./ -type f -name "*.sh~" |xargs rm -f

ENTRYPOINT ["bash","entrypoint.sh"]
